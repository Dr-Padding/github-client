package com.example.githubclient.api

import com.example.githubclient.models.OrganizationRepositories
import com.example.githubclient.models.RepositoryDetails
import retrofit2.Response
import retrofit2.http.GET
import retrofit2.http.Header
import retrofit2.http.Path

interface GithubAPI {
    @GET("/orgs/{org}/repos")
    suspend fun getRepositories(
        @Path("org") org: String,
        @Header("Authorization") token: String,
        @Header("X-GitHub-Api-Version") apiVersion: String
    ): Response<OrganizationRepositories>

    @GET("/repos/{owner}/{repo}")
    suspend fun getRepositoryDetails(
        @Path("owner") owner: String,
        @Path("repo") repo: String
    ): Response<RepositoryDetails>
}
