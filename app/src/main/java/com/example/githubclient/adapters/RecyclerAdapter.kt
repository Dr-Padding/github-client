package com.example.githubclient.adapters

import android.content.Context
import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.AsyncListDiffer
import androidx.recyclerview.widget.DiffUtil
import androidx.recyclerview.widget.RecyclerView
import com.example.githubclient.R
import com.example.githubclient.databinding.OrgRepoItemBinding
import com.example.githubclient.models.OrganizationRepositoriesItem


class RecyclerAdapter(
    private val context: Context,
    private val clickListener: (OrganizationRepositoriesItem) -> Unit
) : RecyclerView.Adapter<RecyclerAdapter.ViewHolder>() {

    private var repoList: ArrayList<OrganizationRepositoriesItem> = ArrayList()

    private val differCallback = object : DiffUtil.ItemCallback<OrganizationRepositoriesItem>() {
        override fun areItemsTheSame(
            oldItem: OrganizationRepositoriesItem,
            newItem: OrganizationRepositoriesItem
        ): Boolean {
            return oldItem.id == newItem.id
        }

        override fun areContentsTheSame(
            oldItem: OrganizationRepositoriesItem,
            newItem: OrganizationRepositoriesItem
        ): Boolean {
            return oldItem == newItem
        }
    }
    private val differ = AsyncListDiffer(this, differCallback)

    fun setNewContent(listOfOrganizationRepositories: ArrayList<OrganizationRepositoriesItem>) {
        repoList = listOfOrganizationRepositories
        differ.submitList(listOfOrganizationRepositories)
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val inflater = LayoutInflater.from(context)
        val binding = OrgRepoItemBinding.inflate(inflater, parent, false)
        return ViewHolder(binding)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val repoItem = repoList[position]
        holder.bind(repoItem, clickListener)
    }

    override fun getItemCount(): Int {
        return differ.currentList.size
    }

    inner class ViewHolder(private val binding: OrgRepoItemBinding) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(
            repoItem: OrganizationRepositoriesItem,
            clickListener: (OrganizationRepositoriesItem) -> Unit
        ) {
            binding.apply {
                tvName.text = repoItem.name
                if (repoItem.description.isNullOrEmpty()) {
                    tvDescription.text = context.getString(R.string.no_description_message)
                } else {
                    tvDescription.text = repoItem.description
                }
                root.setOnClickListener { clickListener(repoItem) }
            }
        }
    }


// это я не забыл, для себя оставил
//    fun clearList() {
//        val size = repoList.size
//        repoList.clear()
//        notifyItemRangeRemoved(0, size)
//    }

}