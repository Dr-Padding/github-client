package com.example.githubclient.repository

import com.example.githubclient.api.RetrofitInstance

class GithubRepository {

    suspend fun getOrganizationRepositories(
        organization: String,
        token: String,
        apiVersion: String
    ) = RetrofitInstance.api.getRepositories(organization, token, apiVersion)

    suspend fun getRepositoryDetails(
        owner: String,
        repository: String
    ) = RetrofitInstance.api.getRepositoryDetails(owner, repository)

}