package com.example.githubclient.ui.fragments

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.githubclient.R
import com.example.githubclient.databinding.FragmentRepositoryDetailsBinding
import com.example.githubclient.repository.GithubRepository
import com.example.githubclient.ui.RepoDetailsViewModel
import com.example.githubclient.ui.ViewModelProviderFactory
import com.example.githubclient.util.Constants.OWNER
import com.example.githubclient.util.Constants.REPOSITORY_NAME


class RepositoryDetailsFragment : Fragment(R.layout.fragment_repository_details) {

    private val binding by viewBinding(FragmentRepositoryDetailsBinding::bind)
    lateinit var viewModel: RepoDetailsViewModel

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViewModel()
        initViews()
        setObservers()
    }

    private fun setUpViewModel() {
        val repository = GithubRepository()
        val viewModelProviderFactory = ViewModelProviderFactory(repository)
        viewModel =
            ViewModelProvider(this, viewModelProviderFactory)[RepoDetailsViewModel::class.java]
    }

    private fun initViews() {
        val owner = arguments?.getString(OWNER)
        val repoName = arguments?.getString(REPOSITORY_NAME)
        if (owner != null) {
            if (repoName != null) {
                viewModel.fetchRepoDetails(owner, repoName)
            }
        }

    }

    private fun setObservers() {
        viewModel.repoDetailsLiveData.observe(viewLifecycleOwner) { repoDetails ->
            if (repoDetails != null) {
                binding.apply {
                    tvRepoName.text = repoDetails.name

                    if (repoDetails.description.isNullOrEmpty()) {
                        tvRepoDesc.text = context?.getString(R.string.no_description_message)
                    } else {
                        tvRepoDesc.text = repoDetails.description
                    }

                    tvNumberOfForks.text = "Forks: ${repoDetails.forks_count}"
                    tvNumberOfWatchers.text = "Watchers: ${repoDetails.watchers_count}"
                    tvOpenIssues.text = "Issues: ${repoDetails.open_issues_count}"

                    if (repoDetails.fork) {
                        tvParentRepoName.text = "Parent: ${repoDetails.parent.full_name}"
                    } else {
                        tvParentRepoName.visibility = View.INVISIBLE
                    }
                }
            }
        }

        viewModel.errorLiveData.observe(viewLifecycleOwner) { errorMessage ->
            if (errorMessage != null) {
                Toast.makeText(requireContext(), errorMessage, Toast.LENGTH_SHORT).show()
            }
        }
    }
}