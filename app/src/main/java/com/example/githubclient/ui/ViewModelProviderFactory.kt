package com.example.githubclient.ui

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import com.example.githubclient.repository.GithubRepository
import java.lang.RuntimeException

class ViewModelProviderFactory(val gitHubRepository: GithubRepository) : ViewModelProvider.Factory {
        override fun <T : ViewModel> create(modelClass: Class<T>): T {
            return when (modelClass) {
                OrgRepoViewModel::class.java -> OrgRepoViewModel(gitHubRepository) as T
                RepoDetailsViewModel::class.java -> RepoDetailsViewModel(gitHubRepository) as T
                else -> throw RuntimeException()
            }
        }

}