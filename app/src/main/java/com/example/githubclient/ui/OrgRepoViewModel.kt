package com.example.githubclient.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.githubclient.models.OrganizationRepositories
import com.example.githubclient.models.RepositoryDetails
import com.example.githubclient.repository.GithubRepository
import com.example.githubclient.util.Constants.API_VERSION
import com.example.githubclient.util.Constants.TOKEN
import kotlinx.coroutines.launch

class OrgRepoViewModel(
    private val gitHubRepository: GithubRepository
) : ViewModel() {

    private val _loadingLiveData = MutableLiveData<Boolean>()
    val loadingLiveData: LiveData<Boolean> = _loadingLiveData

    private val _errorLiveData = MutableLiveData<String?>()
    val errorLiveData: LiveData<String?> = _errorLiveData

    private val _repositoriesLiveData = MutableLiveData<OrganizationRepositories?>()
    val repositoriesLiveData: LiveData<OrganizationRepositories?> = _repositoriesLiveData

    private val _searchQueryLiveData = MutableLiveData<String>()
    val searchQueryLiveData: LiveData<String> = _searchQueryLiveData


    fun fetchRepositories(orgName: String) {
        viewModelScope.launch {
            _loadingLiveData.value = true
            _searchQueryLiveData.value = orgName

            try {
                val response =
                    gitHubRepository.getOrganizationRepositories(orgName, TOKEN, API_VERSION)
                if (response.isSuccessful) {
                    val repositories = response.body()
                    _repositoriesLiveData.value = repositories
                } else {
                    _repositoriesLiveData.value = null
                }
            } catch (e: Throwable) {
                _errorLiveData.value = e.message
            }

            _loadingLiveData.value = false
        }
    }
}