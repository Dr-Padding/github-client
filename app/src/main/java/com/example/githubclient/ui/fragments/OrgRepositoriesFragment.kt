package com.example.githubclient.ui.fragments

import android.os.Bundle
import android.view.View
import android.widget.Toast
import androidx.core.os.bundleOf
import androidx.fragment.app.Fragment
import androidx.lifecycle.ViewModelProvider
import androidx.navigation.fragment.findNavController
import androidx.recyclerview.widget.LinearLayoutManager
import by.kirich1409.viewbindingdelegate.viewBinding
import com.example.githubclient.R
import com.example.githubclient.adapters.RecyclerAdapter
import com.example.githubclient.databinding.FragmentOrgrepositoriesBinding
import com.example.githubclient.models.OrganizationRepositoriesItem
import com.example.githubclient.repository.GithubRepository
import com.example.githubclient.ui.OrgRepoViewModel
import com.example.githubclient.ui.ViewModelProviderFactory
import com.example.githubclient.util.Constants.OWNER
import com.example.githubclient.util.Constants.REPOSITORY_NAME


class OrgRepositoriesFragment : Fragment(R.layout.fragment_orgrepositories) {

    private val binding by viewBinding(FragmentOrgrepositoriesBinding::bind)
    lateinit var mainViewModel: OrgRepoViewModel
    private lateinit var adapter: RecyclerAdapter
    private lateinit var orgName: String

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        setUpViewModel()
        setUpAdapter()
        initViews()
        setObservers()
    }

    private fun setUpViewModel() {
        val repository = GithubRepository()
        val viewModelProviderFactory = ViewModelProviderFactory(repository)
        mainViewModel =
            ViewModelProvider(this, viewModelProviderFactory)[OrgRepoViewModel::class.java]
    }

    private fun initViews() {
        binding.apply {
            btnSearch.setOnClickListener {
                orgName = searchEditText.text.toString()
                if (orgName.isNotEmpty()) {
                    mainViewModel.fetchRepositories(orgName)
                } else {
                    Toast.makeText(requireContext(), R.string.type_orgName, Toast.LENGTH_SHORT)
                        .show()
                }
            }
        }
    }

    private fun setUpAdapter() {
        binding.recyclerView.layoutManager = LinearLayoutManager(requireContext())
        adapter =
            RecyclerAdapter(requireContext(), clickListener = { orgRepoItem ->
                val bundle = bundleOf(
                    OWNER to orgRepoItem.owner.login,
                    REPOSITORY_NAME to orgRepoItem.name
                )
                findNavController().navigate(
                    R.id.action_orgRepositoriesFragment_to_repositoryDetailsFragment,
                    bundle
                )
            })
        binding.recyclerView.adapter = adapter
    }

    private fun setObservers() {
        mainViewModel.loadingLiveData.observe(viewLifecycleOwner) { isLoading ->
            if (isLoading) {
                binding.progressBar.visibility = View.VISIBLE
            } else {
                binding.progressBar.visibility = View.INVISIBLE
            }
        }

        mainViewModel.errorLiveData.observe(viewLifecycleOwner) { errorMessage ->
            if (errorMessage != null) {
                Toast.makeText(requireContext(), errorMessage, Toast.LENGTH_SHORT).show()
            }
        }

        mainViewModel.repositoriesLiveData.observe(viewLifecycleOwner) { repositories ->
            if (repositories != null) {
                adapter.setNewContent(repositories)
            } else {
                adapter.setNewContent(ArrayList<OrganizationRepositoriesItem>())
                Toast.makeText(
                    requireContext(),
                    "No repos found for organization $orgName",
                    Toast.LENGTH_SHORT
                ).show()
            }
        }

        mainViewModel.searchQueryLiveData.observe(viewLifecycleOwner) { orgName ->
            binding.searchEditText.setText(orgName)
        }
    }
}