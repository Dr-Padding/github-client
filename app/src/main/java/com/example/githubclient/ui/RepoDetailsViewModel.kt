package com.example.githubclient.ui

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import androidx.lifecycle.ViewModel
import androidx.lifecycle.viewModelScope
import com.example.githubclient.models.RepositoryDetails
import com.example.githubclient.repository.GithubRepository
import kotlinx.coroutines.launch

class RepoDetailsViewModel(
    private val gitHubRepository: GithubRepository
) : ViewModel() {

    private val _repoDetailsLiveData = MutableLiveData<RepositoryDetails?>()
    val repoDetailsLiveData: LiveData<RepositoryDetails?> = _repoDetailsLiveData

    private val _errorLiveData = MutableLiveData<String?>()
    val errorLiveData: LiveData<String?> = _errorLiveData

    fun fetchRepoDetails(owner: String, repository: String) {
        viewModelScope.launch {
            try {
                val response = gitHubRepository.getRepositoryDetails(owner, repository)
                if (response.isSuccessful) {
                    val repoDetails = response.body()
                    _repoDetailsLiveData.value = repoDetails
                } else {
                    _repoDetailsLiveData.value = null
                }
            } catch (e: Throwable) {
                _errorLiveData.value = e.message
            }
        }
    }
}