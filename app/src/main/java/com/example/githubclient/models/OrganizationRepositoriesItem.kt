package com.example.githubclient.models

data class OrganizationRepositoriesItem(
    val description: String,
    val id: Int,
    val name: String,
    val owner: Owner,
)