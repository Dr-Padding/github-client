package com.example.githubclient.models

data class Parent(
    val full_name: String,
)