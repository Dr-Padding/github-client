package com.example.githubclient.models

data class RepositoryDetails(
    val description: String,
    val fork: Boolean,
    val forks_count: Int,
    val name: String,
    val open_issues_count: Int,
    val parent: Parent,
    val watchers_count: Int,
)